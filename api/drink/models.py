from django.db import models
from api.drink.managers import DrinkManager, MacronutrientDrinkManager, DrinkIntakeManager
from app.models import BaseEntity


class DrinkIntake(models.Model):
    class Meta:
        unique_together = ('user', 'drink', 'date')
        db_table = 'drink_drinkintake'

    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    drink = models.ForeignKey('drink.Drink', on_delete=models.CASCADE)
    quantity = models.FloatField()
    date = models.DateTimeField(auto_now=True)
    objects = DrinkIntakeManager()

    def __str__(self):
        return "{} - {}".format(self.user.username, self.drink.name)

    def is_owner(self, user_id):
        return self.user.id == user_id


class Drink(BaseEntity):
    class Meta:
        ordering = ('id',)
        db_table = 'drink_drink'

    name = models.CharField(max_length=255)
    #macronutrients = models.ManyToManyField('macronutrient.Macronutrient', through='drink.MacronutrientDrink')
    code = models.CharField(max_length=255, null=True, blank=True, unique=True)
    calories = models.FloatField(blank=True, null=True)
    carbohydrates = models.FloatField(blank=True, null=True)
    fat = models.FloatField(blank=True, null=True)
    protein = models.FloatField(blank=True, null=True)
    objects = DrinkManager()


    def __str__(self):
        return self.name


class MacronutrientDrink(models.Model):
    class Meta:
        unique_together = ('drink', 'macronutrient')
        db_table = 'drink_macronutrientdrink'

    drink = models.ForeignKey('drink.Drink', on_delete=models.CASCADE)
    macronutrient = models.ForeignKey('macronutrient.Macronutrient', on_delete=models.CASCADE)
    amount = models.FloatField(blank=False, null=False)
    objects = MacronutrientDrinkManager()

    def __str__(self):
        return "{} - {}".format(self.drink.name, self.macronutrient.name)
