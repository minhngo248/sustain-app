from django.contrib import admin
from .models import Drink,DrinkIntake


admin.site.register(Drink)
admin.site.register(DrinkIntake)