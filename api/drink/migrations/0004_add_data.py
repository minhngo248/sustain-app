from django.db import migrations


def add_data(apps, schema_editor):
    Drink = apps.get_model('drink', 'Drink')
    Macronutrient = apps.get_model('macronutrient', 'Macronutrient')
    MacronutrientDrink = apps.get_model('drink', 'MacronutrientDrink')

    # Create drinks
    drinks = [
        {'name': 'Water', 'code': 'water-111'},
        {'name': 'Coffee', 'code': 'coffee-111'},
        {'name': 'Tea', 'code': 'tea-111'},
        {'name': 'Milk', 'code': 'milk-111'},
        {'name': 'Juice', 'code': 'juice-111'},
        {'name': 'Soda', 'code': 'soda-111'},
        {'name': 'Beer', 'code': 'beer-111'},
        {'name': 'Wine', 'code': 'wine-111'},
    ]
    for drink in drinks:
        Drink.objects.create_drink(**drink)

    # Create macronutrients in drinks
    data = [
        {'drink': Drink.objects.get(name='Milk'), 'macronutrient': Macronutrient.objects.get(name='Carbohydrate'),
            'amount': 12},
        {'drink': Drink.objects.get(name='Milk'), 'macronutrient': Macronutrient.objects.get(name='Protein'),
            'amount': 8},
        {'drink': Drink.objects.get(name='Milk'), 'macronutrient': Macronutrient.objects.get(name='Fat'),
            'amount': 8},
        {'drink': Drink.objects.get(name='Juice'), 'macronutrient': Macronutrient.objects.get(name='Carbohydrate'),
            'amount': 30},
        {'drink': Drink.objects.get(name='Soda'), 'macronutrient': Macronutrient.objects.get(name='Carbohydrate'),
            'amount': 40},
        {'drink': Drink.objects.get(name='Beer'), 'macronutrient': Macronutrient.objects.get(name='Carbohydrate'),
            'amount': 13},
        {'drink': Drink.objects.get(name='Beer'), 'macronutrient': Macronutrient.objects.get(name='Protein'),
            'amount': 1},
        {'drink': Drink.objects.get(name='Wine'), 'macronutrient': Macronutrient.objects.get(name='Carbohydrate'),
            'amount': 4},
    ]

    for d in data:
        MacronutrientDrink.objects.create_macronutrient_drink(**d)


class Migration(migrations.Migration):

        dependencies = [
            ('drink', '0003_auto_20230708_1733'),
            ('macronutrient', '0003_add_data'),
        ]

        operations = [
            migrations.RunPython(add_data),
        ]
