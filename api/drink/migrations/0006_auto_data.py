from django.db import migrations

def fill_drink_data(apps, schema_editor):
    Drink = apps.get_model('drink', 'Drink')
    
    # Sample data for drinks
    drinks_data = [
        {'name': 'Water', 'code': 'water', 'calories': 0, 'carbohydrates': 0, 'fat': 0, 'protein': 0},
        {'name': 'Protein Shake', 'code': 'protein-shake', 'calories': 150, 'carbohydrates': 10, 'fat': 3, 'protein': 25},
        {'name': 'Orange Juice', 'code': 'orange-juice', 'calories': 110, 'carbohydrates': 26, 'fat': 0.5, 'protein': 1},
        {'name': 'Sports Drink', 'code': 'sports-drink', 'calories': 80, 'carbohydrates': 21, 'fat': 0, 'protein': 0},
        {'name': 'Iced Tea', 'code': 'iced-tea', 'calories': 45, 'carbohydrates': 11, 'fat': 0, 'protein': 0},
        {'name': 'Chocolate Milk', 'code': 'chocolate-milk', 'calories': 190, 'carbohydrates': 26, 'fat': 5, 'protein': 8},
        {'name': 'Coffee', 'code': 'coffee', 'calories': 5, 'carbohydrates': 0, 'fat': 0, 'protein': 0},
        {'name': 'Lemonade', 'code': 'lemonade', 'calories': 120, 'carbohydrates': 30, 'fat': 0, 'protein': 0.5},
        {'name': 'Apple Juice', 'code': 'apple-juice', 'calories': 100, 'carbohydrates': 24, 'fat': 0, 'protein': 0.5},
        {'name': 'Coconut Water', 'code': 'coconut-water', 'calories': 45, 'carbohydrates': 9, 'fat': 0, 'protein': 0.5},
    ]

    
    for data in drinks_data:
        Drink.objects.create(**data)

def remove_drink_data(apps, schema_editor):
    # If needed, you can add a reverse operation to remove the data.
    pass

class Migration(migrations.Migration):

    dependencies = [
        ('drink', '0005_auto_20230723_1132'),
    ]

    operations = [
        migrations.RunPython(fill_drink_data, remove_drink_data),
    ]
