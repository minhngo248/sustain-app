from rest_framework import serializers
from api.drink.models import Drink, MacronutrientDrink, DrinkIntake


class DrinkByUserSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255)
    code = serializers.CharField(max_length=255)
    quantity = serializers.FloatField()
    date = serializers.DateTimeField()


class RankingDrinkByCaloriesSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=255)
    code = serializers.CharField(max_length=255)
    calories = serializers.FloatField()


class DrinkIntakeSerializer(serializers.ModelSerializer):
    class Meta:
        model = DrinkIntake
        #fields = "__all__"
        depth = 0
        #fields = ["id", "drink", "quantity", "date"]
        read_only_fields = ("id",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        request = self.context.get("request")
        if request and request.method == "GET":
            self.Meta.depth = 1
            self.Meta.exclude = None
            self.Meta.fields= '__all__'
        else:
            self.Meta.depth = 0
            self.Meta.exclude = ["user"]
            self.Meta.fields= None

    # Custom read-only field for Calories
    calories = serializers.SerializerMethodField()

    def get_calories(self,obj):
        if obj.quantity:
            drink = Drink.objects.get(id=obj.drink.id)
            drink_calories = drink.calories
            calories = drink_calories * obj.quantity / 100
        else:
            calories = 0
    def create(self, validated_data):
        user = self.context["request"].user
        drink_intake = DrinkIntake.objects.create_drink_intake(user, **validated_data)
        return drink_intake

    def update(self, instance, validated_data):
        instance = DrinkIntake.objects.update_drink_intake(instance, **validated_data)
        return instance


class DrinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drink
        fields = "__all__"
        read_only_fields = ("id",)

    def create(self, validated_data):
        drink = Drink.objects.create_drink(**validated_data)
        return drink

    def update(self, instance, validated_data):
        instance = Drink.objects.update_drink(instance, **validated_data)
        return instance


class MacronutrientDrinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = MacronutrientDrink
        fields = ["id", "drink", "macronutrient", "amount"]
        read_only_fields = ("id",)

    def create(self, validated_data):
        macronutrient_drink = MacronutrientDrink.objects.create_macronutrient_drink(**validated_data)
        Drink.objects.add_macronutrient_to_drink(macronutrient_drink.drink, macronutrient_drink.macronutrient)
        return macronutrient_drink

    def update(self, instance, validated_data):
        instance = MacronutrientDrink.objects.update_macronutrient_drink(instance, **validated_data)
        return instance
