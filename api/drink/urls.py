from django.urls import path
from api.drink.views import (
    DrinkList,
    DrinkDetail,
    MacronutrientDrinkList,
    MacronutrientDrinkDetail,
    DrinkIntakeList,
    DrinkIntakeDetail,
    DrinkTakenByUser,
    DrinkTakenByUserDate,
    RankingDrinkByCalories
)

urlpatterns = [
    path("", DrinkList.as_view({'get': 'list', 'post': 'create'})),
    path("ranking/", RankingDrinkByCalories.as_view({'get': 'list'})),
    path("<int:pk>/", DrinkDetail.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path("<int:drink_id>/macronutrients/", MacronutrientDrinkList.as_view({'get': 'list', 'post': 'create'})),
    path("<int:drink_id>/macronutrients/<int:pk>/", MacronutrientDrinkDetail.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path("drink-intake/", DrinkIntakeList.as_view({'get': 'list', 'post': 'create'})),
    path("drink-intake/<int:pk>/", DrinkIntakeDetail.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path("drink-intake/user/", DrinkTakenByUser.as_view({'get': 'list'})),
    path("drink-intake/user/date/", DrinkTakenByUserDate.as_view({'get': 'list'})),
]
