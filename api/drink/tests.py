from rest_framework.test import APITestCase, APIClient
from api.macronutrient.models import Macronutrient
from api.drink.models import Drink, MacronutrientDrink
from api.user.models import User
from rest_framework.authtoken.models import Token


class DrinkTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        list_macronutrients = [
            {
                "name": "Protein",
                "calories_per_gram": 4
            },
            {
                "name": "Carbohydrate",
                "calories_per_gram": 4
            },
            {
                "name": "Fat",
                "calories_per_gram": 9
            },
            {
                "name": "Alcohol",
                "calories_per_gram": 7
            }
        ]
        for macronutrient in list_macronutrients:
            Macronutrient.objects.create_macronutrient(**macronutrient)
        apple_juice = {
            "name": "Apple Juice",
            "code": "apple-juice-111"
        }
        Drink.objects.create_drink(**apple_juice)
        cls.apple_juice = Drink.objects.get(name="Apple Juice")
        carbohydrate = Macronutrient.objects.get(name="Carbohydrate")
        MacronutrientDrink.objects.create_macronutrient_drink(
            drink=cls.apple_juice,
            macronutrient=carbohydrate,
            amount=11.3
        )
        Drink.objects.add_macronutrient_to_drink(drink=cls.apple_juice, macronutrient=carbohydrate)

        cocacola = {
            "name": "Coca Cola",
            "code": "coca-cola-111"
        }
        Drink.objects.create_drink(**cocacola)
        cls.cocacola = Drink.objects.get(name="Coca Cola")
        carbohydrate = Macronutrient.objects.get(name="Carbohydrate")
        MacronutrientDrink.objects.create_macronutrient_drink(
            drink=cls.cocacola,
            macronutrient=carbohydrate,
            amount=10.6
        )
        Drink.objects.add_macronutrient_to_drink(drink=cls.cocacola, macronutrient=carbohydrate)

    def setUp(self) -> None:
        user_data = {
            "username": "admin"
        }
        user = User.objects.create_user(is_superuser=True, **user_data)
        # Create token
        client = APIClient()
        token = Token.objects.create(user=user)
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        client.force_authenticate(user=user)
        self.client = client

    def test_add_coca_duplicate(self):
        data = {
            "name": "Coca Cola",
            "code": "coca-cola-111"
        }
        response = self.client.post("/api/v1/drinks/", data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data, {
            "code": [
                "drink with this code already exists."
            ]
        })
