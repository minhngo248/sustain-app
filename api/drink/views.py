from django.http import JsonResponse, HttpResponse
from rest_framework.parsers import JSONParser
from app.views import (
    ListView,
    RetrieveView,
    CreateView,
    UpdateView,
    DestroyView,
)
from api.drink.serializers import DrinkSerializer, MacronutrientDrinkSerializer, DrinkIntakeSerializer, \
    DrinkByUserSerializer, RankingDrinkByCaloriesSerializer
from api.drink.models import Drink, MacronutrientDrink, DrinkIntake
from rest_framework import status
from django.db import transaction
from app.permissions import UserCanOnlyRead, IsOwner
from rest_framework.permissions import IsAdminUser, IsAuthenticated


class DrinkTakenByUser(ListView):
    serializer_class = DrinkByUserSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        drink_intakes = DrinkIntake.objects.filter(user=user)
        return drink_intakes


class DrinkTakenByUserDate(ListView):
    serializer_class = DrinkIntakeSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        date = self.request.query_params.get('date', None)
        if date: drink_intakes = DrinkIntake.objects.filter(user=user).filter(date__date=date)
        else: drink_intakes = DrinkIntake.objects.filter(user=user)

        return drink_intakes


class DrinkIntakeDetail(RetrieveView, UpdateView, DestroyView):
    serializer_class = DrinkIntakeSerializer
    queryset = DrinkIntake.objects.all()
    permission_classes = [IsOwner]

    def get(self, request, *args, **kwargs):
        serializer = DrinkIntakeSerializer(request.drink_intake)
        return JsonResponse(serializer.data)

    def put(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        serializer = DrinkIntakeSerializer(request.drink_intake, data=data)
        with transaction.atomic():
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        request.drink_intake.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


class DrinkIntakeList(ListView, CreateView):
    serializer_class = DrinkIntakeSerializer
    queryset = DrinkIntake.objects.all()
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return DrinkIntake.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        serializer = DrinkIntakeSerializer(data=data)
        with transaction.atomic():
            if serializer.is_valid():
                serializer.save(user=request.user)
                return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DrinkDetail(RetrieveView, UpdateView, DestroyView):
    class Meta:
        model = Drink
        fields = ('id', 'name', 'code', 'macronutrients')
        read_only_fields = ('id',)

    queryset = Drink.objects.all()
    serializer_class = DrinkSerializer
    permission_classes = [UserCanOnlyRead | IsAdminUser]

    def get(self, request, *args, **kwargs):
        serializer = DrinkSerializer(request.drink)
        return JsonResponse(serializer.data)

    def put(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        serializer = DrinkSerializer(request.drink, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        Drink.objects.delete_drink(request.drink)
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


class DrinkList(ListView, CreateView):
    class Meta:
        model = Drink
        fields = ('id', 'name', 'code')
        read_only_fields = ('id',)

    serializer_class = DrinkSerializer
    permission_classes = [UserCanOnlyRead | IsAdminUser]

    def get_queryset(self):
        return Drink.objects.all()

    def post(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        serializer = DrinkSerializer(data=data)
        with transaction.atomic():
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RankingDrinkByCalories(ListView):
    serializer_class = RankingDrinkByCaloriesSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        results = MacronutrientDrink.objects.rank_drink_by_calories()
        return results


class MacronutrientDrinkDetail(RetrieveView, UpdateView, DestroyView):
    queryset = MacronutrientDrink.objects.all()
    serializer_class = MacronutrientDrinkSerializer
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        serializer = MacronutrientDrinkSerializer(request.macronutrient_drink)
        return JsonResponse(serializer.data)

    def put(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        serializer = MacronutrientDrinkSerializer(request.macronutrient_drink, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        Drink.objects.remove_macronutrient_from_drink(request.macronutrient_drink.drink,
                                                      request.macronutrient_drink.macronutrient)
        MacronutrientDrink.objects.delete_macronutrient_drink(request.macronutrient_drink)
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


class MacronutrientDrinkList(ListView, CreateView):
    serializer_class = MacronutrientDrinkSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        serializer = MacronutrientDrinkSerializer(MacronutrientDrink.objects.all(), many=True)
        return serializer.data

    def post(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        serializer = MacronutrientDrinkSerializer(data=data)
        with transaction.atomic():
            if serializer.is_valid():
                serializer.save()
                return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
