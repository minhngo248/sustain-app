from django.db import models
from rest_framework import exceptions
import datetime
import pytz
from django.db import connection


class DrinkManager(models.Manager):
    use_in_migrations = True
    def get_drink_by_id(self, drink_id):
        if not self.filter(id=drink_id).exists():
            raise exceptions.ValidationError("Drink with id {} does not exist".format(drink_id))
        drink = self.get(id=drink_id)
        return drink

    def get_drinks(self):
        drinks = self.all()
        return drinks

    def create_drink(self, **data):
        try:
            name = data.pop('name')
        except KeyError:
            raise exceptions.ValidationError("Name is required")
        drink = self.create(name=name, **data)
        return drink

    def update_drink(self, drink, **data):
        drink.name = data.get('name', drink.name)
        drink.code = data.get('code', drink.code)
        drink.updated_at = datetime.datetime.now()
        drink.save()
        return drink

    def delete_drink(self, drink):
        drink.delete()
        return drink

    def add_macronutrient_to_drink(self, drink, macronutrient):
        drink.macronutrients.add(macronutrient)
        return drink

    def remove_macronutrient_from_drink(self, drink, macronutrient):
        drink.macronutrients.remove(macronutrient)
        return drink


class MacronutrientDrinkManager(models.Manager):
    use_in_migrations = True
    def create_macronutrient_drink(self, **data):
        try:
            drink = data.pop('drink')
            macronutrient = data.pop('macronutrient')
            amount = data.pop('amount')
        except KeyError:
            raise exceptions.ValidationError("Drink, macronutrient and amount are required")
        macronutrient_drink = self.filter(drink=drink, macronutrient=macronutrient)
        if macronutrient_drink.exists():
            raise exceptions.ValidationError("Drink already has this macronutrient")
        macronutrient_drink = self.create(drink=drink, macronutrient=macronutrient, amount=amount)
        return macronutrient_drink

    def update_macronutrient_drink(self, macronutrient_drink, **data):
        macronutrient_drink.drink = data.get('drink', macronutrient_drink.drink)
        macronutrient_drink.macronutrient = data.get('macronutrient', macronutrient_drink.macronutrient)
        macronutrient_drink.amount = data.get('amount', macronutrient_drink.amount)
        macronutrient_drink.save()
        return macronutrient_drink

    def delete_macronutrient_drink(self, macronutrient_drink):
        macronutrient_drink.delete()
        return macronutrient_drink

    def rank_drink_by_calories(self):
        query = """
                SELECT drink_drink.id, drink_drink.name, drink_drink.code, SUM(drink_macronutrientdrink.amount * macronutrient_macronutrient.calories_per_gram) AS calories
                FROM drink_drink, drink_macronutrientdrink, macronutrient_macronutrient
                WHERE drink_drink.id = drink_macronutrientdrink.drink_id AND drink_macronutrientdrink.macronutrient_id = macronutrient_macronutrient.id
                GROUP BY drink_drink.id, drink_drink.name, drink_drink.code
                ORDER BY calories DESC
                """
        with connection.cursor() as cursor:
            cursor.execute(query)
            drinks = cursor.fetchall()
        label = ["id", "name", "code", "calories"]
        drinks = [dict(zip(label, drink)) for drink in drinks]
        return drinks


class DrinkIntakeManager(models.Manager):
    def get_drink_taken_by_user_on_date(self, user, date=None):
        if date is not None:
            date = datetime.datetime.strptime(date, '%Y-%m-%d')
            date_start = datetime.datetime(date.year, date.month, date.day, 0, 0, 0, tzinfo=pytz.utc)
            date_end = datetime.datetime(date.year, date.month, date.day, 23, 59, 59, tzinfo=pytz.utc)
            query = """
                SELECT drink_drink.name, drink_drink.code, drink_drinkintake.quantity, drink_drinkintake.date
                FROM drink_drink, drink_drinkintake
                WHERE drink_drink.id = drink_drinkintake.drink_id AND drink_drinkintake.user_id = %s AND drink_drinkintake.date BETWEEN %s AND %s
                ORDER BY drink_drinkintake.date DESC
                """
            with connection.cursor() as cursor:
                cursor.execute(query, [user.id, date_start, date_end])
                drink_intake = cursor.fetchall()
        else:
            query = """
                SELECT drink_drink.name, drink_drink.code, drink_drinkintake.quantity, drink_drinkintake.date
                FROM drink_drink, drink_drinkintake
                WHERE drink_drink.id = drink_drinkintake.drink_id AND drink_drinkintake.user_id = %s
                ORDER BY drink_drinkintake.date DESC
                """
            with connection.cursor() as cursor:
                cursor.execute(query, [user.id])
                drink_intake = cursor.fetchall()
        label = ["name", "code", "quantity", "date"]
        result = [dict(zip(label, drink)) for drink in drink_intake]
        return result

    def create_drink_intake(self, user, **data):
        try:
            print(f"-------------\n{data}\n-------------")
            drink = data.pop('drink')
            quantity = data.pop('quantity')
        except KeyError:
            raise exceptions.ValidationError("Drink and quantity are required")
        drink_intake = self.create(user=user, Drink=drink, quantity=quantity)
        return drink_intake

    def update_drink_intake(self, drink_intake, **data):
        drink_intake.drink = data.get('drink', drink_intake.drink)
        drink_intake.amount = data.get('quantity', drink_intake.quantity)
        drink_intake.date = data.get('date', drink_intake.date)
        drink_intake.save()
        return drink_intake

    def delete_drink_intake(self, drink_intake):
        drink_intake.delete()
        return drink_intake
