from django.urls import path
from api.activity.views import (
    ActivityListView,
    ActivityDetailView,
    ActivityTakenByUser,
    ActivityTakenListView,
    ActivityTakenDetailView,
)

urlpatterns = [
    path("", ActivityListView.as_view({'get': 'list', 'post': 'create'})),
    path("<int:pk>/", ActivityDetailView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'})),
    path("taken/", ActivityTakenByUser.as_view({'get': 'list'})),
    path("user/", ActivityTakenListView.as_view({'post': 'create'})),
    path("user/<int:pk>/", ActivityTakenDetailView.as_view({'delete': 'destroy'})),
]
