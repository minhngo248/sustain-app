from django.contrib import admin

from .models import Activity,ActivityTaken


admin.site.register(Activity)
admin.site.register(ActivityTaken)