from django.http import JsonResponse, HttpResponse
from rest_framework.parsers import JSONParser
from app.views import (
    ListView,
    RetrieveView,
    CreateView,
    UpdateView,
    DestroyView,
)
from api.activity.serializers import ActivitySerializer, ActivityByUserSerializer, ActivityTakenSerializer
from api.activity.models import Activity, ActivityTaken
from rest_framework import status
from app.permissions import UserCanOnlyRead, IsOwner
from rest_framework.permissions import IsAdminUser, IsAuthenticated


class ActivityListView(ListView, CreateView):
    serializer_class = ActivitySerializer
    permission_classes = [UserCanOnlyRead | IsAdminUser]

    def get_queryset(self):
        return Activity.objects.all()

    def post(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        serializer = ActivitySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )


class ActivityDetailView(RetrieveView, UpdateView, DestroyView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = [UserCanOnlyRead | IsAdminUser]

    def get(self, request, *args, **kwargs):
        serializer = ActivitySerializer(request.activity)
        return JsonResponse(serializer.data)

    def put(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        serializer = ActivitySerializer(request.activity, data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data)
        return JsonResponse(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )

    def delete(self, request, *args, **kwargs):
        Activity.objects.delete_activity(request.activity)
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


class ActivityTakenDetailView(DestroyView):
    queryset = ActivityTaken.objects.all()
    serializer_class = ActivityTakenSerializer
    permission_classes = [IsOwner]

    def delete(self, request, *args, **kwargs):
        ActivityTaken.objects.delete_activity_taken(request.activity_taken)
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


class ActivityTakenByUser(ListView):
    serializer_class = ActivityByUserSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        result = ActivityTaken.objects.get_activity_by_user(user.id)
        return result


class ActivityTakenListView(CreateView):
    serializer_class = ActivityTakenSerializer
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        data = JSONParser().parse(request)
        serializer = ActivityTakenSerializer(data=data)
        if serializer.is_valid():
            serializer.save(user=request.user)
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(
            serializer.errors,
            status=status.HTTP_400_BAD_REQUEST
        )
