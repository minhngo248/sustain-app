from django.db import migrations

def fill_activity_data(apps, schema_editor):
    Activity = apps.get_model('activity', 'Activity')
    
    # Sample data for activities
    activities_data = [
      #  {'name': 'Running','description': 'Running',  'calories_burned': 10},
        {'name': 'Cycling','description': 'Cycling',  'calories_burned': 8},
        {'name': 'Swimming','description': 'Swimming',  'calories_burned': 12},
        {'name': 'Jump Rope', 'description': 'jump-rope', 'calories_burned': 15},
        {'name': 'Yoga','description': 'Yoga',  'calories_burned': 5},
        {'name': 'Basketball','description': 'Basketball',  'calories_burned': 12},
        {'name': 'Football','description': 'Football',  'calories_burned': 10},
        {'name': 'Tennis','description': 'Tennis',  'calories_burned': 9},
        {'name': 'Dancing','description': 'Dancing',  'calories_burned': 8},
        {'name': 'Hiking','description': 'Hiking',  'calories_burned': 7},
        {'name': 'Weightlifting','description': 'Weightlifting',  'calories_burned': 6},
        {'name': 'Rowing','description': 'Rowing',  'calories_burned': 12},
        {'name': 'Soccer','description': 'Soccer',  'calories_burned': 11},
        {'name': 'Pilates','description': 'Pilates',  'calories_burned': 6},
        {'name': 'Volleyball','description': 'Volleyball',  'calories_burned': 9},
        {'name': 'Gardening','description': 'Gardening',  'calories_burned': 5},
        {'name': 'Rock Climbing', 'description': 'rock-climbing', 'calories_burned': 14},
        {'name': 'Martial Arts', 'description': 'martial-arts', 'calories_burned': 13},
        {'name': 'Skating','description': 'Skating',  'calories_burned': 7},
    ]
    
    for data in activities_data:
        Activity.objects.create(**data)

def remove_activity_data(apps, schema_editor):
    # If needed, you can add a reverse operation to remove the data.
    pass

class Migration(migrations.Migration):

    dependencies = [
        ('activity', '0005_alter_activity_name'),
    ]

    operations = [
        migrations.RunPython(fill_activity_data, remove_activity_data),
    ]
