from django.db import models
from rest_framework import exceptions
from django.db import connection
import datetime


class ActivityManager(models.Manager):
    def create_activity(self, **data):
        try:
            name = data.pop('name')
        except KeyError:
            raise exceptions.ValidationError("Name is required")
        if self.filter(name=name).exists():
            raise exceptions.ValidationError("Activity already exists")
        activity = self.create(name=name, **data)
        return activity

    def update_activity(self, activity, **data):
        activity.name = data.get('name', activity.name)
        activity.description = data.get('description', activity.description)
        activity.calories_burned = data.get('calories_burned', activity.calories_burned)
        activity.updated_at = datetime.datetime.now()
        activity.save()
        return activity

    def delete_activity(self, activity):
        activity.delete()
        return True


class ActivityTakenManager(models.Manager):
    def create_activity_taken(self, user, **data):
        try:
            activity = data.pop('activity')
            duration = data.pop('duration')
        except KeyError:
            raise exceptions.ValidationError("Activity and duration are required")
        activity_taken = self.create(user=user, activity=activity, duration=duration)
        return activity_taken

    def delete_activity_taken(self, activity_taken):
        activity_taken.delete()
        return True

    def update_activity_taken(self, activity_taken, **data):
        activity_taken.duration = data.get('duration', activity_taken.duration)
        activity_taken.save()
        return activity_taken

    def delete_activity_taken(self, activity_taken):
        activity_taken.delete()
        return True

    def get_activity_by_user(self, user_id):
        query = """
            SELECT activity_activity.name, activity_activity.description, activity_activity.calories_burned, 
            activity_taken.duration, activity_taken.date_taken
            FROM activity_activity, activity_taken
            WHERE activity_activity.id = activity_taken.activity_id AND activity_taken.user_id = %s
            ORDER BY activity_taken.date_taken DESC
            """
        cursor = connection.cursor()
        cursor.execute(query, [user_id])
        activities = cursor.fetchall()
        label = ["name", "description", "calories_burned", "duration", "date_taken"]
        result = [dict(zip(label, activity)) for activity in activities]
        return result
