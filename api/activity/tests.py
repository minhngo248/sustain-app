from rest_framework.test import APITestCase, APIClient
from rest_framework.authtoken.models import Token
from api.activity.models import Activity
from api.user.models import User


class ActivityTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        activity_data = {
            "name": "Running"
        }
        Activity.objects.create_activity(**activity_data)

    def setUp(self) -> None:
        user_data = {
            "username": "admin"
        }
        user = User.objects.create_superuser(**user_data)
        # Create token
        client = APIClient()
        token = Token.objects.create(user=user)
        client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)
        client.force_authenticate(user=user)
        self.client = client

    def test_add_activity_name_duplicate(self):
        activity_data = {
            "name": "Running"
        }
        response = self.client.post('/api/v1/activities/', activity_data)
        self.assertEqual(response.status_code, 400)

    def test_add_activity_name_empty(self):
        activity_data = {
            "name": ""
        }
        response = self.client.post('/api/v1/activities/', activity_data)
        self.assertEqual(response.status_code, 400)

    def test_add_activity_success(self):
        activity_data = {
            "name": "Walking"
        }
        response = self.client.post('/api/v1/activities/', activity_data)
        self.assertEqual(response.status_code, 201)
