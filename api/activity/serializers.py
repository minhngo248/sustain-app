from rest_framework import serializers
from api.activity.models import Activity, ActivityTaken


class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ["id", "name", "description", "calories_burned", "created_at", "updated_at"]
        read_only_fields = ("id",)

    def create(self, validated_data):
        activity = Activity.objects.create_activity(**validated_data)
        return activity

    def update(self, instance, validated_data):
        instance = Activity.objects.update_activity(instance, **validated_data)
        return instance


class ActivityTakenSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivityTaken
        fields = ["id", "activity", "duration"]
        read_only_fields = ("id",)

    def create(self, validated_data):
        user = self.context["request"].user
        activity_taken = ActivityTaken.objects.create_activity_taken(user, **validated_data)
        return activity_taken

    def update(self, instance, validated_data):
        instance = ActivityTaken.objects.update_activity_taken(instance, **validated_data)
        return instance


class ActivityByUserSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255)
    description = serializers.CharField(max_length=255)
    calories_burned = serializers.FloatField()
    duration = serializers.FloatField()
    date_taken = serializers.DateTimeField()
