from django.db import models
from app.models import BaseEntity
from api.activity.managers import ActivityManager, ActivityTakenManager


class Activity(BaseEntity):
    name = models.CharField(max_length=255, blank=False, null=False, unique=True)
    description = models.TextField(blank=True, null=True)
    calories_burned = models.FloatField(blank=True, null=True)
    objects = ActivityManager()

    class Meta:
        ordering = ["-created_at", "-id"]
        db_table = "activity_activity"


class ActivityTaken(models.Model):
    user = models.ForeignKey("user.User", on_delete=models.CASCADE)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)
    duration = models.FloatField(blank=False, null=False)
    date_taken = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    objects = ActivityTakenManager()

    class Meta:
        db_table = "activity_taken"

    def is_owner(self, user_id):
        return self.user.id == user_id
